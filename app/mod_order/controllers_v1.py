from flask import Blueprint, jsonify, request, render_template
from flask_api import status
from flask_jwt import current_identity, jwt_required
from passlib.hash import pbkdf2_sha512

from app import db, user_datastore
from app.mod_auth.models import User
from app.mod_order.models import Category, Product, Order

from sqlalchemy import or_
from datetime import datetime, timedelta
from uuid import uuid4
import jwt

mod_order_v1 = Blueprint('order_v1', __name__, url_prefix='/v1.0/order')


@mod_order_v1.route('/category', methods=['GET'])
# @jwt_required()
def category_get():
    categories = Category.query.with_entities(
        Category.id,
        Category.extra['name'].label('name'),
    ).filter(
        Category.active,
    ).order_by(Category.extra['name'])

    return jsonify(
        items=list(map(lambda category: {
            'id': category.id,
            'name': category.name
        }, categories))
    )


@mod_order_v1.route('/category/<int:_id>', methods=['GET'])
def category_get_id(_id: int):
    category = Category.query.filter(
        Category.id == _id,
        Category.active
    ).first()

    if category:
        return jsonify(
            item={
                'id': category.id,
                'name': category.extra['name'],
                'color': category.extra['color'],
                'products': list(map(lambda product: {
                    'id': product.id,
                    'name': product.extra['name']
                }, category.products))
            }
        )
    else:
        return jsonify(
            msg='Categoria não existe.'
        )


@mod_order_v1.route('/category', methods=['POST'])
def category_post():
    category = Category()
    data = request.json
    category.extra = data

    db.session.add(category)

    db.session.commit()

    return jsonify(
        id=category.id,
        msg='Categoria criada com sucesso.'
    )


@mod_order_v1.route('/category/<int:_id>', methods=['PUT'])
def category_put_id(_id: int):
    category = Category.query.filter(
        Category.id == _id,
        Category.active
    ).first()

    if category:
        data = request.json
        category.extra = data

        db.session.commit()

        return jsonify(
            id=category.id,
            msg='Categoria atualizada com sucesso.'
        )
    else:
        return jsonify(
            msg='Categoria não existe'
        )


@mod_order_v1.route('/category/<int:_id>', methods=['DELETE'])
def category_delete_id(_id: int):
    category = Category.query.filter(
        Category.id == _id,
        Category.active
    ).first()

    if category:
        category.active = False

        db.session.commit()

        return jsonify(
            id=category.id,
            msg='Categoria excluida com sucesso.'
        )
    else:
        return jsonify(
            msg='Categoria não existe'
        )


@mod_order_v1.route('/product', methods=['POST'])
def product_post():
    product = Product()
    data = request.json

    product.category_id = data['category_id']
    del data['category_id']

    product.extra = data

    db.session.add(product)

    db.session.commit()

    return jsonify(
        id=product.id,
        msg='Produto criado com sucesso.'
    )
